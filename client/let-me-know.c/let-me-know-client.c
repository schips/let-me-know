/*
#    Copyright By Schips, All Rights Reserved
#    https://gitee.com/schips/
#
#    File Name:  client.c
#    Created  :  Sat 21 Mar 2020 04:43:39 PM CST
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "sconfig.h"

#define CONFIG_INIT ".client.ini"
#define MAX_PATH_LEN 256
char config_file[MAX_PATH_LEN];

struct notify_message {
    int  token;
    char tittle[128];
    char message[128];
    int  duration;
    char type[128];
    char url[128];
};

int let_me_know(Config * conf, char *section)
{
    static struct notify_message buff;
    // 指定地址
    struct sockaddr_in addr = {0};
    struct item* host, *port, *token, *msg, *tlt, *type, *duration, *url;
    static int my_socket;
    int ret;
    struct section *cur_section = NULL;

    cur_section = find_section_in_config(conf, section);
    if(!cur_section)
    {
        printf("Item [%s] not found in %s\n", section, config_file);
        return -1;
    }

    memset(&buff, 0, sizeof(buff));
    host = port = NULL;
    msg = tlt =  type =  duration = NULL;

    host     = sconfig_get_item_from_section(conf, section, "host");
    port     = sconfig_get_item_from_section(conf, section, "port");

    tlt      = sconfig_get_item_from_section(conf, section, "tittle");
    msg      = sconfig_get_item_from_section(conf, section, "msg");
    type     = sconfig_get_item_from_section(conf, section, "type");
    duration = sconfig_get_item_from_section(conf, section, "duration");
    token    = sconfig_get_item_from_section(conf, section, "token");
    url      = sconfig_get_item_from_section(conf, section, "url");

    if(!host || !port ) {
        printf("config not found\n");
        return -1;
    }

    // 标题，不填时，默认是 Let Me Know
    if(tlt)
        sprintf(buff.tittle, "%s", (char *)sconfig_get_item_val(tlt));
    else
        sprintf(buff.tittle, "Let Me Know");

    // 消息正文
    if(msg)
        sprintf(buff.message, "%s", (char *)sconfig_get_item_val(msg));
    else
        sprintf(buff.message, "OK");

    // 类型，代表 是正常消息还是警告，还是错误,实际上对应的是 一张图片
    if(type)
        sprintf(buff.type, "%s", (char *)sconfig_get_item_val(type));
    else
        sprintf(buff.type, "info");

    // 弹窗持续时间, 默认10秒
    if(duration)
        buff.duration = atoi(sconfig_get_item_val(duration));
    else
        buff.duration = 10;
    if(token)
        buff.token = atoi(sconfig_get_item_val(token));

    // 提醒默认打开的链接，默认为空
    if(url)
        sprintf(buff.url, "%s", (char *)sconfig_get_item_val(url));
    else
        buff.url[0] = '\0';

    // 创建套接字
    my_socket = socket(AF_INET, SOCK_STREAM, 0); // IPV4, TCP socket
    if(my_socket == -1) 
    { 
        perror("Socket"); 
        return -1;
    }
	
    addr.sin_family = AF_INET; 	// 地址协议族 
	addr.sin_addr.s_addr = inet_addr(sconfig_get_item_val(host));   //指定 IP地址
	addr.sin_port = htons(atoi(sconfig_get_item_val(port))); //指定端口号

    ret = connect(my_socket, (struct sockaddr *)(&addr), sizeof(struct sockaddr_in));
    if(-1 == ret) { perror("connect"); }

    send(my_socket, &buff, sizeof(buff), 0);

    return close(my_socket); perror("close");
}

int main(int argc, char *argv[])
{
    DECLARE_CONFIG(conf);
    int i = 1;

    //获取当前程序所在目录的绝对路径
    char current_absolute_path[MAX_PATH_LEN];
    int cnt = readlink("/proc/self/exe", current_absolute_path, MAX_PATH_LEN);
    if (cnt < 0 || cnt >= MAX_PATH_LEN)
    {
        printf("fuck\n");
        return -1;
    }
    //获取当前目录绝对路径，即去掉程序名
    for (i = cnt; i >=0; --i)
    {
        if (current_absolute_path[i] == '/')
        {
            current_absolute_path[i+1] = '\0';
            break;
        }
    }
    sprintf(config_file, "%s/%s", current_absolute_path, CONFIG_INIT);

    sconfig_init(&conf,config_file);

    if(argc == 1)
        let_me_know(&conf, "default");
    else
    {
        i = 1;
        while(i < argc)
        {
            let_me_know(&conf, argv[i]);
            i++;
            sleep(1);
        }
    }

    return 0;
}

