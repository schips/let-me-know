# Let-Me-Know

一个跨平台的通知小工具。

## 背景

有些时候等待编译是比较久的一件事情。比如此时在进行某次庞大的编译/构建；另外一边进行并行的开发，总是会因为多余的等待而浪费了时间。

`Let-Me-Know`正是为了解决这个问题的！

## 用法

流程如下。

### 服务器

开启服务器进程。

执行Server，能够在桌面环境中看到一个小爱心的图标♥。

### 客户端

1、修改 `.client.ini`，进行下发配置，与需要发送的信息。

| 配置项   | 意义                                                       | 默认            |
| -------- | ---------------------------------------------------------- | --------------- |
| host     | 服务器地址                                                 | 不允许为空      |
| token    | 通信口令                                                   | 不允许为空      |
| port     | 端口号                                                     | 不允许为空      |
| tittle   | 通知消息标题                                               | Let me know     |
| msg      | 通知消息主消息                                             | NULL            |
| duration | 通知消息显示时间（单位，秒）                               | 10秒            |
| type     | 通知消息类型，此属性会导致server读取对应的图片作为logo     | NULL |
| url     | 通知消息链接，当此属性不为空时，点击server对应的提示窗口，会尝试打开对应的链接| NULL |

例如：

```bash
[case]
host = 127.0.0.1
port = 16666
token=20200516
tittle='Let me know'
msg = 'A default message'
# 弹窗持续时间，0代表永久, 单位是秒
duration = 0
type=info
url="https://gitee.com/schips"
```

2、以这样的形式进行执行命令：

```bash
./some-harvy-cmd ; let-me-know [case] ...
```

> case 指的是上述的一组配置，实际上代表了某个可能使用到的场景，如果不指定，默认使用`default`
>
> 这些场景以 ini配置项 的形式写入 .client.ini 中，随时可以编辑。

这样就实现了通知的功能。此时就可以回到对应的终端进行检查。

## 特性

### 定时提醒

初衷：用于提醒女朋友不要成为“久坐族”。

基于crontab的简化配置思想，允许自定义配置提醒事项。

### 远程提醒

#### Server

驻留在后台的进程，当收到Client的通知请求后进行通知。

> 由 QT5 + TCP 实现。

server 可以配置是否接受允许远程提醒，是否在接受远程提醒的同时打开其指定的url


#### Client

每次执行client的进程，则会通知server。

> Linux C 与 Python3都可以用于执行通知。

## 最后

特别感谢：
 - https://gitee.com/cassfrontend/qt-notify
 - https://www.iconfont.cn/
