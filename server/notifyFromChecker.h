﻿#ifndef NOTIFYFROMCONFIGCHECKER_H
#define NOTIFYFROMCONFIGCHECKER_H

#include <QObject>
#include <QTimer>
#include <QString>
#include <QSettings>
#include "fileSystemWatcher.h"
#include "notifyFromCheckerPeriodicConfig.h"


struct ConfigItem {
    /* Debug Only */
    QString Name;
    QString Description;
    QString Enable;
    /* Windows show*/
    QString Message;
    QString Tittle;
    int During;
    QString Type;
    /* For alarm*/
    QStringList AlarmMonth;
    QStringList AlarmWek;
    QStringList AlarmHour;
    QStringList AlarmMin;
    QStringList AlarmSec;
    /* Advance features */
    QString URL;
};

/*! \enum FLAG_FOR_CONFIG
 *
 *  Detailed description
 */
enum FLAG_FOR_CONFIG {
    MON_OK  = 0x01,
    WEK_OK  = 0x02,
    HOUR_OK = 0x04,
    MIN_OK  = 0x08,
    SEC_OK  = 0x10,
};
class notifyFromChecker: public QObject
{
    Q_OBJECT

public slots:
    void CheckForNoifty(void);

signals:
    void showNotifyWindows(QString tittle, QString msg, QString & url, int  durationSec, QString type);

private:
    void initTimer(int period_seconds);

    int mPeriodSeconds;
    QTimer *timer1sForCheckingNotify;
    bool mIsEnable;

    struct ConfigItem *mConfigItem;
    int mFactorsCnt;


public:
    notifyFromChecker();
    void CheckFromItems( struct ConfigItem * items, int itemCnt);
    void setEnable(bool logic);
    bool getEnable(void);
};

#endif // NOTIFYFROMCONFIGCHECKER_H
