﻿
#include <QDesktopWidget>
#include <QPropertyAnimation>
#include <QApplication>
#include <QDebug>
#include <QTimer>

#include "notifyWidgetManager.h"

const int RIGHT = 10;
const int BOTTOM = 10;

static NotifyManager *notifyManager = nullptr;


NotifyManager *NotifyManager::getInstance(void)
{
    return notifyManager;
}

void NotifyManager::setInstance(NotifyManager * Manager)
{
    notifyManager = Manager;
}

NotifyManager::NotifyManager(QObject *parent): QObject(parent),
    displayTime(6555 * 1000)
{
    mtopConfig = topConfig::getInstance();
    mWinHeight = mtopConfig->getNotifyWindowsHeight();
    mWinWidth = mtopConfig->getNotifyWindowsWidth();
    mWinGap = mtopConfig->getNotifyWindowsGap();
    maxCount = mtopConfig->getMaxNotifyWindowsCount();
    setInstance(this);
}


void NotifyManager::showSystemMessage(QString & tittleArg, QString & bodyArg, QString & url, int durationSec, QString type)
{
    QString application_path = QApplication::applicationDirPath();
    application_path = application_path.replace(QString('\\'), QString('/'));

    if(durationSec <= 0) durationSec = 0 ;
    //qDebug() << application_path + "\\images\\"+ type + ".png";
    notify(tittleArg, bodyArg, application_path + "/images/"+ type + ".png", url, durationSec * 1000);
}


void NotifyManager::notify(const QString &title, const QString &body, const QString &icon, const QString url, int displayTime)
{
    QDateTime timeMark = QDateTime::currentDateTime();
    //qDebug() << "notify";
    dataQueue.enqueue(NotifyData(icon, title, body, url, displayTime, timeMark));
    showNext();
}

void NotifyManager::setMaxCount(int count)
{
    maxCount = count;
}

void NotifyManager::setDisplayTime(int ms)
{
    displayTime = ms;
}

// 调整所有提醒框的位置
void NotifyManager::rearrange()
{
    QDesktopWidget *desktop = QApplication::desktop();
    QRect desktopRect = desktop->availableGeometry();
    QPoint bottomRignt = desktopRect.bottomRight();

    QList<Notify*>::iterator i;
    for (i = notifyList.begin(); i != notifyList.end(); ++i) {
        int index = notifyList.indexOf((*i));

        QPoint pos = bottomRignt - QPoint(mWinWidth + RIGHT, (mWinHeight + mWinGap) * (index + 1) - mWinGap + BOTTOM);
        QPropertyAnimation *animation = new QPropertyAnimation((*i), "pos", this);
        animation->setStartValue((*i)->pos());
        animation->setEndValue(pos);
        animation->setDuration(300);
        animation->start();

        connect(animation, &QPropertyAnimation::finished, this, [animation, this](){
            animation->deleteLater();
        });
    }
}


void NotifyManager::showNext()
{
    if(notifyList.size() >= maxCount || dataQueue.isEmpty()) {
        return;
    }

    NotifyData data = dataQueue.dequeue();
    Notify *notify = new Notify();
    //qDebug() << data.displayTime;
    notify->setIcon(data.icon);
    notify->setTitle(data.title);
    notify->setBody(data.body);
    notify->setUrl(data.url);
    notify->setDisplayTime(data.displayTime);
    notify->setFixedSize(mWinWidth, mWinHeight);
    notify->setNotifyTime(data.timeMark);

    QDesktopWidget *desktop = QApplication::desktop();
    QRect desktopRect = desktop->availableGeometry();

    // 计算提醒框的位置
    QPoint bottomRignt = desktopRect.bottomRight();
    QPoint pos = bottomRignt - QPoint(notify->width() + RIGHT, (mWinHeight + mWinGap) * (notifyList.size() + 1) - mWinGap + BOTTOM);


    notify->move(pos);
    notify->showGriant();
    notifyList.append(notify);

    connect(notify, &Notify::disappeared, this, [notify, this](){
        this->notifyList.removeAll(notify);
        this->rearrange();

        // 如果列表是满的，重排完成后显示
        if(this->notifyList.size() == this->maxCount - 1){
            QTimer::singleShot(300, this, [this]{
               this->showNext();
            });
        } else {
            this->showNext();
        }
        notify->deleteLater();
    });
}

