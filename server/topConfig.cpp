﻿#include "topConfig.h"
#include <QApplication>
#include <QDebug>
#include <QFile>
const int DefaultRemoteNotifyBindPort = 16666; // 端口号

const int DefaultMaxNotifyWindowsCount = 3; // 同时显示的最大窗口数量
const int DefaultWinHeight = 100; // 窗口高度
const int DefaultWinWidth = 300;  // 窗口宽度
const int DefaultWinSpace = 15;   // 窗口之间的间距
const int DefaultLogoHeight = 40;  // 图标高度
//const int DefaultLogoWidth  = 40;  // 图标宽度

const int DefaultTittleHeight = 100; // 标题高度
//const int DefaultBodyHeight = 100; // 正文高度


//const int DefaultTittleFontSize = 14;  // 标题的字体大小
//const int DefaultBodyFontSize = 14;    // 正文的字体大小

int topConfig::getTocConfigValue(QString value, int defaultValue)
{
    int ret;
    QSettings *mConfig = new QSettings(mTopConfigFile, QSettings::IniFormat);
    mConfig->setIniCodec("UTF-8");
    ret = mConfig->value(value).toInt();
    delete mConfig;
    if(ret == 0)  ret = defaultValue;

    return ret;
}

void topConfig::reload(void)
{
    FileSystemWatcher * watcher = FileSystemWatcher::getInstance();
    if(watcher)
    {
        watcher->PruneFileWithoutWatch(mTopConfigFile);
    }
    QSettings *mConfig = new QSettings(mTopConfigFile, QSettings::IniFormat);
    mConfig->setIniCodec("UTF-8");
    //qDebug() << "let-me-know.ini is : [" + mTopConfigFile + "]";

    // 自定义的提醒配置
    mNotifyConfigPath  = mConfig->value("config/notify").toString();
    mNotifyConfigPath  = mNotifyConfigPath.replace(QString('\\'), QString('/'));
    if(watcher)
    {
        watcher->PruneFileWithoutWatch(mNotifyConfigPath);
    }
    // 最大显示窗口数
    mMaxNotifyWindowsCount =  getTocConfigValue("config/MaxNotifyWindowsCount", DefaultMaxNotifyWindowsCount);
    // 自定义CSS
    mNotifyCSSPath   = mConfig->value("config/CssFile").toString();
    mNotifyCSSPath   = mNotifyCSSPath.replace(QString('\\'), QString('/'));

    mWinWidth    = getTocConfigValue("config/WindowsWidth", DefaultWinWidth);
    mWinHeight   = getTocConfigValue("config/WindowsHeight", DefaultWinHeight);
    mWinGap      = getTocConfigValue("config/WindowsGap", DefaultWinSpace);
    // 字体通过CSS配置
    //mTittleFontSize  = getTocConfigValue("config/TittleFontSize", DefaultTittleFontSize);
    //mBodyFontSize    = getTocConfigValue("config/BodyFontSize", DefaultBodyFontSize);
    // 自适配；高度可以定制，但是默认的宽度等于图标宽度以外的内容
    mLogoHeight  = getTocConfigValue("config/LogoHeight", DefaultLogoHeight);
    mLogoWidth   = getTocConfigValue("config/LogoWidth",  mLogoHeight);
    mTittleHeight  = getTocConfigValue("config/TittleLabelHeight", DefaultTittleHeight - mWinGap);
    mTittleWidth   = getTocConfigValue("config/TittleLabelWidth", mWinWidth - mLogoWidth);
    mBodyHeight  = getTocConfigValue("config/BodyLabelHeight", mWinHeight - mTittleHeight);
    mBodyWidth   = getTocConfigValue("config/BodyLabelWidth", mWinWidth - mLogoWidth);

    mFootHeight  = getTocConfigValue("config/FootLabelHeight", mWinHeight - mBodyHeight- mTittleHeight);
    mFootWidth   = getTocConfigValue("config/FootLabelWidth", mWinWidth - mLogoWidth);
    // 绑定端口号
    mBindPort   = getTocConfigValue("config/RemoteNotifyBindPort", DefaultRemoteNotifyBindPort);
    // 远程口令
    mRemoteToken= getTocConfigValue("config/RemoteNotifyToken", 0);

    /* 是否允许 远程提醒 */
    if(QString::compare(tr("no"),
                        mConfig->value("config/EnableRemoteNotify").toString(),
                        Qt::CaseInsensitive)==0)
    {
        mIsEnableRemoteNotify = false;
    }else
    {
        mIsEnableRemoteNotify = true;
    }
    /* 是否允许远程提醒时，打开对应的URL*/
    if(QString::compare(tr("no"),
                        mConfig->value("config/EnableRemoteNotifyURL").toString(),
                        Qt::CaseInsensitive)==0)
    {
        mIsEnableRemoteNotifyURL = false;
    }else
    {
        mIsEnableRemoteNotifyURL = true;
    }

#if 0
    qDebug() << "mNotifyConfigPath  : [" << mNotifyConfigPath  << "]";
    qDebug() << "mNotifyCSSPath     : [" << mNotifyCSSPath  << "]";

    qDebug() << "mMaxNotifyWindowsCount  : [" << mMaxNotifyWindowsCount  << "]";

    qDebug() << "mWinWidth  : [" << mWinWidth  << "]";
    qDebug() << "mWinHeight : [" << mWinHeight << "]";
    qDebug() << "mWinGap    : [" << mWinGap    << "]";

    qDebug() << "mTittleWidth  : [" << mTittleWidth    << "]";
    qDebug() << "mTittleHeight : [" << mTittleHeight << "]";

    qDebug() << "mBodyHeight  : [" << mBodyHeight    << "]";
    qDebug() << "mBodyWidth : [" << mBodyWidth << "]";

    qDebug() << "mLogoHeight : [" << mLogoHeight << "]";
    qDebug() << "mLogoWidth  : [" << mLogoWidth    << "]";

    qDebug() << "mFootHeight : [" << mFootHeight << "]";
    qDebug() << "mFootWidth  : [" << mFootWidth    << "]";

    qDebug() << "mBindPort  : [" << mBindPort    << "]";
    qDebug() << "mRemoteToken  : [" << mRemoteToken    << "]";

    qDebug() << "mIsEnableRemoteNotify is" << mIsEnableRemoteNotify;
    qDebug() << "mIsEnableRemoteNotifyURL is" << mIsEnableRemoteNotifyURL;
#endif
    delete mConfig;
}


int topConfig::getMaxNotifyWindowsCount(void)
{
    return mMaxNotifyWindowsCount;
}

int topConfig::getNotifyWindowsHeight(void)
{
    return mWinHeight;
}

int topConfig::getNotifyWindowsWidth(void)
{
    return mWinWidth;
}

int topConfig::getNotifyWindowsGap(void)
{
    return mWinGap;
}

int topConfig::getTittleHeight(void)
{
    //qDebug() << "getTittleHeight :" << mTittleHeight;
    return mTittleHeight;
}

int topConfig::getTittleWidth(void)
{
    //qDebug() << "getTittleWidth :" << mTittleWidth;
    return mTittleWidth;
}

int topConfig::getBodyHeight(void)
{
    //qDebug() << "getBodyHeight :" << mBodyHeight;
    return mBodyHeight;
}

int topConfig::getBodyWidth(void)
{
    //qDebug() << "getBodyWidth :" << mBodyHeight;
    return mBodyWidth;
}

int topConfig::getFootHeight(void)
{
    //qDebug() << "getFootHeight :" << mFootHeight;
    return mFootHeight;
}

int topConfig::getFootWidth(void)
{
    //qDebug() << "getFootWidth :" << mFootWidth;
    return mFootWidth;
}

int topConfig::getLogoHeight(void)
{
    return mLogoHeight;
}
int topConfig::getLogoWidth(void)
{
    return mLogoWidth;
}

uint16_t topConfig::getBindPort(void)
{
    return mBindPort;
}

int topConfig::getRemoteToken(void)
{
    return mRemoteToken;
}

bool topConfig::isEnableRemoteNotify(void)
{
    return mIsEnableRemoteNotify;
}

bool topConfig::isEnableRemoteNotifyURL(void)
{
    return mIsEnableRemoteNotifyURL;
}
topConfig::topConfig(void)
{
    QString application_path = QApplication::applicationDirPath();
    application_path = application_path.replace(QString('\\'), QString('/'));
    mTopConfigFile = application_path + "/let-me-know.ini";
    FileSystemWatcher::getInstance()->addWatchPath(mTopConfigFile);
    topConfig::setInstance(this);
    reload();
}
static topConfig* instance = nullptr;

topConfig* topConfig::getInstance()
{
    if(instance != nullptr)
    {
        //instance->reload();
    }
    return instance;
}

void topConfig::setInstance(topConfig* cfg)
{
    instance = cfg;
}

#include <QFileInfo>
QString topConfig::getCustNotifyConfigPath(void)
{
    QFileInfo fileInfo(mNotifyConfigPath);
    QString filepath = fileInfo.absoluteFilePath();
    filepath.replace(QString('\\'), QString('/'));
    qDebug()<< "getCustNotifyConfigPath" << filepath;
    return filepath;
    //QString application_path = QApplication::applicationDirPath();
    //application_path = application_path.replace(QString('\\'), QString('/'));
    //return application_path + "/"+ mNotifyConfigPath;
}


QString topConfig::getTopConfigPath(void)
{
    return mTopConfigFile;
}

QString topConfig::getNotifyCSSPath(void)
{
    QString application_path = QApplication::applicationDirPath();
    application_path = application_path.replace(QString('\\'), QString('/'));
    return application_path + "/" + mNotifyCSSPath;
}
