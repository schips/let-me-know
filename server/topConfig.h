﻿#if 1
#ifndef topConfig_H
#define topConfig_H

#include <QObject>
#include <QSettings>
#include "fileSystemWatcher.h"
#if 0
[config]

; 使用的 提醒配置（只允许相对路径，路径使用'/'表示）
notify=custom-notify.ini

;;;;;;;;;;;;;;;;;;;;;;;;;; 关于窗口外观设置 ;;;;;;;;;;;;;;;;;;;;;;;;;;
; 窗口大小
;; 宽度
WindowsWidth=300
;; 高度
WindowsHeight=100
;; 窗口之间的间隙
WindowsGap=15

; 标签大小（不可以超过窗口大小）
;; 宽度
LabelWidth=300
;; 高度
LabelHeight=100
;;;;;;;;;;;;;;;;;;;;;;;;;; 关于窗口内部的字体设置 ;;;;;;;;;;;;;;;;;;;;;;;;;;
FontSize=14


;;;;;;;;;;;;;;;;;;;;;;;;;; 是否启动远程通知（关闭输入NO） ;;;;;;;;;;;;;;;;;;;;;;;;;;
EnableRemoteNotify=yes
EnableRemoteNotifyURL=yes
#endif

class topConfig : public QObject
{
    Q_OBJECT
public:
    //explicit topConfig(QObject *parent = nullptr);
    topConfig();
    static topConfig* getInstance();
    static void setInstance(topConfig* cfg);
    void reload(void);
    QString getCustNotifyConfigPath(void);

    QString getTopConfigPath(void);
    // 获取路径
    QString getNotifyCSSPath(void);
    uint16_t getBindPort(void);
    int getRemoteToken(void);
    bool isEnableRemoteNotify(void);
    bool isEnableRemoteNotifyURL(void);
    int getMaxNotifyWindowsCount(void);
    // 主窗口大小
    int getNotifyWindowsHeight(void);
    int getNotifyWindowsWidth(void);
    int getNotifyWindowsGap(void);
    // 标签大小
    // 标题
    int getTittleHeight(void);
    int getTittleWidth(void);
    // 正文
    int getBodyHeight(void);
    int getBodyWidth(void);
    // 脚注：时间戳
    int getFootHeight(void);
    int getFootWidth(void);
    // 图标大小
    int getLogoHeight(void);
    int getLogoWidth(void);
    // 字体大小的设置，请参考css


signals:

public slots:
private:


    ~topConfig() {};
    topConfig(const topConfig&);
    topConfig& operator=(const topConfig&);
    int getTocConfigValue(QString value, int defaultValue);
    /* let-me-know.ini */
    QString mTopConfigFile;

    /* Notify Config file*/
    QString mNotifyConfigPath;
    /* CSS Path*/
    QString mNotifyCSSPath;


    /* Feature */
    uint16_t mBindPort;
    int mRemoteToken;
    bool mIsEnableRemoteNotify;
    bool mIsEnableRemoteNotifyURL;
    int mMaxNotifyWindowsCount;
    /* Windows show */
    int  mWinWidth;
    int  mWinHeight;
    int  mWinGap;
    /* Labels show*/
    // Tittle
    int mTittleHeight;
    int mTittleWidth;
    // Body
    int mBodyHeight;
    int mBodyWidth;
    /* Foot: timemark */
    int mFootHeight;
    int mFootWidth;

    /* Logo show */
    int  mLogoWidth;
    int  mLogoHeight;

    /* For Text*/
    //int  mTittleFontSize;
    //int  mBodyFontSize;
};

#endif // INI_H
#endif

