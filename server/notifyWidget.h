#ifndef NOTIFYWIDGET_H
#define NOTIFYWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QMouseEvent>
#include <QPushButton>
#include <qdatetime.h>
#include "notifyFromChecker.h"
#include "topConfig.h"
enum CloseReason {
    // 如果是点击窗体，视为接受这个提醒，会打开URL（默认）
    mouseClickAsAccept,
    // 点击关闭按钮，则视为忽略，不打开URL
    closeBtnAsIgnore,
};

void runURl(QString urlOri);

class Notify : public QWidget
{
    Q_OBJECT
public:
    explicit Notify(QWidget *parent = 0);

    void setIcon(const QString &value);

    void setTitle(const QString &value);

    void setBody(const QString &value);

    void setUrl(const QString &value);

    void setDisplayTime(int value);

    void setNotifyTime(QDateTime value);

    void showGriant();

Q_SIGNALS:
    void disappeared();

private:

    int displayTime;

    QDateTime timeMark;

    QString icon;
    QString title;
    QString body;
    QString url;

    QLabel *backgroundLabel;
    QLabel *iconLabel;
    QLabel *titleLabel;
    QLabel *bodyLabel;
    QLabel *footLabel;

    QPushButton *closeBtn;

    CloseReason mCloseReason;

    void hideGriant();

    void mousePressEvent(QMouseEvent *event);
    notifyFromChecker *mconfigChecker;
    topConfig *mtopConfig;
};

#endif // NOTIFYWIDGET_H
