﻿#include <QDebug>
#include <QFile>
#include <QDir>
#include <QFileInfo>
#include "notifyFromCheckerPeriodicConfig.h"
#include "topConfig.h"

static notifyFromCheckerPeriodicConfig * perioldConfig = nullptr;
notifyFromCheckerPeriodicConfig::notifyFromCheckerPeriodicConfig()
{
    mIniConfig = nullptr;
    mNotifyItem = nullptr;
    mNotifyItemCnt = 0;
    //mCustNotifyConfigFile = topConfig::getInstance()->getCustNotifyConfigPath();
    reloadPeriodicConfig();
    notifyFromCheckerPeriodicConfig::setInstance(this);
}

notifyFromCheckerPeriodicConfig * notifyFromCheckerPeriodicConfig::getInstance(void)
{
    return perioldConfig;
}

void notifyFromCheckerPeriodicConfig::setInstance(notifyFromCheckerPeriodicConfig * cfg)
{
    perioldConfig = cfg;
}

int notifyFromCheckerPeriodicConfig::getNotifyCnt(void)
{
    return mNotifyItemCnt;
}

struct ConfigItem * notifyFromCheckerPeriodicConfig::getNotifyItems(void)
{
    return mNotifyItem;
}

void notifyFromCheckerPeriodicConfig::clearNotifyItems(void)
{
    if(mNotifyItemCnt != 0)
        delete []mNotifyItem;
    mNotifyItemCnt = 0;
}

void notifyFromCheckerPeriodicConfig::reloadPeriodicConfigFromFile(const QString & file)
{
    int i = 0;
    if(mIniConfig) delete mIniConfig;

    FileSystemWatcher::getInstance()->PruneFileWithoutWatch(file);

    mIniConfig = new QSettings(file, QSettings::IniFormat);
    mIniConfig->setIniCodec("UTF-8");
    qDebug() << "CustNotify.config : [" + file + "]";

    QStringList groupList=mIniConfig->childGroups();
    if(mNotifyItemCnt != 0)
        delete []mNotifyItem;

    mNotifyItemCnt = groupList.count();
    if(mNotifyItemCnt == 0)
        goto end;
    mNotifyItem = new struct ConfigItem[mNotifyItemCnt];

    i = 0;
    foreach(QString group,groupList)
    {
        mIniConfig->beginGroup(group);

        mNotifyItem[i].Name        = group;
        mNotifyItem[i].Description = mIniConfig->value("Description").toString();
        mNotifyItem[i].Enable      = mIniConfig->value("Enable").toString();
        mNotifyItem[i].Message     = mIniConfig->value("Message").toString();
        mNotifyItem[i].Tittle      = mIniConfig->value("Tittle").toString();
        mNotifyItem[i].Type        = mIniConfig->value("Type").toString();
        mNotifyItem[i].During      = mIniConfig->value("During").toInt();
        mNotifyItem[i].AlarmMonth  = mIniConfig->value("AlarmMonth").toStringList();
        mNotifyItem[i].AlarmWek    = mIniConfig->value("AlarmWek").toStringList();
        mNotifyItem[i].AlarmHour   = mIniConfig->value("AlarmHour").toStringList();
        mNotifyItem[i].AlarmMin    = mIniConfig->value("AlarmMin").toStringList();
        mNotifyItem[i].AlarmSec    = mIniConfig->value("AlarmSec").toStringList();
        mNotifyItem[i].URL         = mIniConfig->value("URL").toString();
        //mNotifyItem[i].URL = QDir::toNativeSeparators(mNotifyItem[i].URL);
        //qDebug() <<mNotifyItem[i].URL;
#if 0
        //qDebug() << mIniConfig->value("URL");

        if(QString::compare(tr(""), mNotifyItem[i].URL, Qt::CaseInsensitive))
        {
            emit showNotifyWindows(mNotifyItem[i].Tittle, mNotifyItem[i].Message,
                                   mNotifyItem[i].URL,
                                   mNotifyItem[i].During, mNotifyItem[i].Type);
            qDebug() << "showNotifyWindows "<< "[" << mNotifyItem[i].Name << "]";
        }
#endif
        mIniConfig->endGroup();
        i++;
    }

#if 0
    for (i = 0; i < mNotifyItemCnt; ++i)
    {
        qDebug() << "//////////////////////////////";
        qDebug() << mNotifyItem[i].Name;
        qDebug() << mNotifyItem[i].Description;
        qDebug() << mNotifyItem[i].Enable;
        qDebug() << mNotifyItem[i].Message;
        qDebug() << mNotifyItem[i].Tittle;
        qDebug() << mNotifyItem[i].During;
        qDebug() << mNotifyItem[i].Type;
        qDebug() << mNotifyItem[i].AlarmMonth   ;
        qDebug() << mNotifyItem[i].AlarmWek     ;
        qDebug() << mNotifyItem[i].AlarmHour    ;
        qDebug() << mNotifyItem[i].AlarmMin     ;
        qDebug() << mNotifyItem[i].AlarmSec     ;
        qDebug() << mNotifyItem[i].URL     ;
        qDebug() << "";
    }
#endif
end:
    return;
}

// 依次从目录下读取对应的ini文件（目录仅一级，不递归遍历）
void notifyFromCheckerPeriodicConfig::reloadPeriodicConfigFromDirLevel1(const QString& dirPath)
{
    QDir dir(dirPath);
    dir.setFilter(QDir::Files | QDir::NoSymLinks| QDir::NoDotAndDotDot);

    foreach(QFileInfo fileInfo, dir.entryInfoList())
    {
        //if(fullDir.fileName() == "." || fullDir.fileName() == "..") continue;
        qDebug() << fileInfo.absoluteFilePath();
        reloadPeriodicConfigFromFile(fileInfo.absoluteFilePath());
    }
    return ;
}


// 从提供的path（目录或文件）读取配置
void notifyFromCheckerPeriodicConfig::reloadPeriodicConfig(void)
{
    mCustNotifyConfigFile = topConfig::getInstance()->getCustNotifyConfigPath();

    QFileInfo pathInfo(mCustNotifyConfigFile);
    if(mCustNotifyConfigFile == "")
    {
        qDebug() << "reloadPeriodicConfig but null";
        return;
    }
    if(pathInfo.isFile())
    {
        qDebug()<< "Trying reloadPeriodicConfig from file:" << mCustNotifyConfigFile;
        clearNotifyItems();
        reloadPeriodicConfigFromFile(mCustNotifyConfigFile);
    }else if(pathInfo.isDir())
    {
        qDebug()<< "Trying reloadPeriodicConfig from dir:" << mCustNotifyConfigFile;
        clearNotifyItems();
        reloadPeriodicConfigFromDirLevel1(mCustNotifyConfigFile);
    }else {
        qDebug() << "Trying reloadPeriodicConfig but unknow";
    }
}
