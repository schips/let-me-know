﻿#ifndef FILE_SYSTEM_WATCHER_H
#define FILE_SYSTEM_WATCHER_H

#include <QObject>
#include <QMap>
#include <QFileSystemWatcher>
#include <QTimer>

class FileSystemWatcher : public QObject
{
    Q_OBJECT

public:
    explicit FileSystemWatcher(QObject *parent = nullptr);
    static void setInstance(FileSystemWatcher * watcher);
    static FileSystemWatcher * getInstance(void);
    void addWatchPath(QString &path);
    void delWatchPath(QString &path);
    void stopWatchPathForAWhile(QString &path, int mseconds = 2000);
    void PruneFileWithoutWatch(QString ReadFilePath);
    bool isPathInWatch(QString &path);
signals:
    void NotifyConfigFileUpdated(QString path);
public slots:
    void directoryUpdated(const QString &path);  // 目录更新时调用，path是监控的路径
    void fileUpdated(const QString &path);   // 文件被修改时调用，path是监控的路径
    void timeoutAndReAdd(void);
private:
    QFileSystemWatcher *m_pSystemWatcher;  // QFileSystemWatcher变量
    QMap<QString, QStringList> m_currentContentsMap; // 当前每个监控的内容目录列表
    QStringList addedPaths;
    QStringList pendingPathsToAdd;
    QTimer *m_timerForReAdd;

};

#endif // FILE_SYSTEM_WATCHER_H
