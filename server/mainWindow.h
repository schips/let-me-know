﻿/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QSystemTrayIcon>

#ifndef QT_NO_SYSTEMTRAYICON

#include <QMainWindow>

#include <QAction>
#include <QTableView>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include "notifyFromCheckerPeriodicConfig.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE
QT_BEGIN_NAMESPACE
class QComboBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QMenu;
class QPushButton;
class QSpinBox;
class QTextEdit;
QT_END_NAMESPACE

#include "notifyFromTcpServer.h"
#include "notifyWidgetManager.h"
#include "notifyFromChecker.h"
#include "topConfig.h"
#include "fileSystemWatcher.h"

#define MAIN_MAX_BTN 5

struct btn {
    QString name;
    QPushButton   *itemBtn;

};

class Window;
union for_callback {
    //c_fun fun;
    void *(*fun)(void *);
    void *(Window::*func)(void);
};

class Window : public QMainWindow
{
    Q_OBJECT

public:
    Window(QWidget *parent = nullptr);

    void setVisible(bool visible) override;
Q_SIGNALS:
    void disappeared();
signals:
    //void messageIn(QString &meg);
    void click_main_btn(int index);
protected:
    void closeEvent(QCloseEvent *event) override;

private slots:
    //int OnSystemTrayClicked(QSystemTrayIcon::ActivationReason reason);
    void setIcon(void);
    void iconActivated(QSystemTrayIcon::ActivationReason reason);

    void showSystemMessage(QString & tittleArg, QString & bodyArg, QString & url, int durationSec, QString type);

    void showMegs(QString tittle, QString msg, QString url, int  durationSec, QString type);
    int  suspendForWhile(bool checked);
    void SetMyAppAutoRun(bool isstart);
    void reloadConfig(void);
    void modifyConfig(void);

    void on_DebugTabPushButtonDemo_released();


    void on_DebugTabPushButtonEditCustomINI_released();

    void on_DebugTabPushButtonEditTop_released();

    void on_DebugTabPushButtonEditCSS_released();

private:
    Ui::MainWindow *ui;
    topConfig* mtopConfig;
    notifyFromCheckerPeriodicConfig* m_PeriodicConfig;
    notifyFromChecker *mconfigChecker;
    void createTrayIconActions();
    void createTrayIcon();

    QLabel *backgroundLabel;
    QPushButton   *closeBtn;
    QPushButton   *itemBtn[MAIN_MAX_BTN];
    QTableView    *tabView;

    QGroupBox *iconGroupBox;
    QLabel *iconLabel;
    QComboBox *iconComboBox;

    QVBoxLayout *mVBLayout;
    QHBoxLayout *mHBLayout;

    QGroupBox *messageGroupBox;
    //QLabel *typeLabel;
    //QLabel *durationLabel;
    //QLabel *durationWarningLabel;
    //QLabel *titleLabel;
    //QLabel *bodyLabel;
    //QComboBox *typeComboBox;
    //QSpinBox *durationSpinBox;
    //QLineEdit *titleEdit;
    //QTextEdit *bodyEdit;
    //QPushButton *showMessageButton;

    QAction *enableAction;
    QAction *autoBootAction;
    QAction *reloadConfigAction;
    QAction *modifyConfigAction;
    QAction *aboutMeAction;
    QAction *quitAction;

    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;
    notifyFromTcpServer *tcpserver;
    NotifyManager *manager;
    void notify_config_file_update(QString path);

    union for_callback fp;


    FileSystemWatcher *mFileSystemWatcher;
    QString mTopConfigFile;
    QString mCustNotifyFile;
    QString mSchedulePath;
};
//! [0]

#endif // QT_NO_SYSTEMTRAYICON

#endif
