﻿#ifndef NOTIFYFROMCHECKERPERIODICCONFIG_H
#define NOTIFYFROMCHECKERPERIODICCONFIG_H

#include <QObject>
#include <QSettings>
#include "notifyFromChecker.h"
#if 0
[workday-for-water-15]
; 描述
Description='星期1到星期5的喝水提醒'
; 弹窗内容
Message='^(*￣(oo)￣)^起来喝水！'
Tittle='^(*￣(oo)￣)^起来喝水！'
; 弹窗持续时间，单位s，（300 = 5 分钟）
During=300
; 提醒类型
; 什么时候提醒，每月
AlarmMonth=1,2,3,4,5,6,7,8,9,10,11,12
; 星期几提醒, 星期1到星期5
AlarmWek=mon,tues,wed,thur,fri
; 什么小时开始
AlarmHour=9,10,11,13,14,15,16,17,18
; 哪一分钟
AlarmMin=30
; 哪一秒
AlarmSec=0

#endif

class notifyFromCheckerPeriodicConfig
{
public:
    notifyFromCheckerPeriodicConfig();
    struct ConfigItem *getNotifyItems(void);
    int getNotifyCnt(void);
    //void reload(void);
    void reloadPeriodicConfig(void);
    static notifyFromCheckerPeriodicConfig *getInstance(void);

private:
    static void setInstance(notifyFromCheckerPeriodicConfig * cfg);
    void clearNotifyItems(void);
    void reloadPeriodicConfigFromFile(const QString & file);
    void reloadPeriodicConfigFromDirLevel1(const QString& dirPath);
    QSettings *mIniConfig;
    // 自定义配置
    QString mCustNotifyConfigFile;
    int mNotifyItemCnt;
    struct ConfigItem * mNotifyItem;
};

#endif // NOTIFYFROMCHECKERPERIODICCONFIG_H
