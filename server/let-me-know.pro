HEADERS       = \
                fileSystemWatcher.h \
                mainWindow.h \
                notifyFromChecker.h \
                notifyFromCheckerPeriodicConfig.h \
                notifyFromTcpServer.h \
                topConfig.h
SOURCES       = main.cpp \
                fileSystemWatcher.cpp \
                mainWindow.cpp \
                notifyFromChecker.cpp \
                notifyFromCheckerPeriodicConfig.cpp \
                notifyFromTcpServer.cpp \
                topConfig.cpp
RESOURCES     = \
                icon.qrc

# 指定图标
RC_ICONS = let-me-know.ico
# 设置程序版本号，可省略
VERSION = 1.1.0.0

SOURCES += \
                notifyWidget.cpp \
                notifyWidgetManager.cpp

HEADERS += \
                notifyWidget.h \
                notifyWidgetManager.h



QT += widgets network
requires(qtConfig(combobox))

Debug:DESTDIR = $$OUT_PWD
Debug:OBJECTS_DIR = $$OUT_PWD/debug/.obj
Debug:MOC_DIR = $$OUT_PWD/debug/.moc
Debug:RCC_DIR = $$OUT_PWD/debug/.rcc
Debug:UI_DIR = $$OUT_PWD/debug/.ui

Release:DESTDIR = $$OUT_PWD
Release:OBJECTS_DIR = $$OUT_PWD/release/.obj
Release:MOC_DIR = $$OUT_PWD/release/.moc
Release:RCC_DIR = $$OUT_PWD/release/.rcc
Release:UI_DIR = $$OUT_PWD/release/.ui

copy_dir.files = $$PWD/images
copy_dir.path = $$OUT_PWD
copy_files.files = $$PWD/custom-notify.ini $$PWD/let-me-know.ini $$PWD/let-me-know.css
copy_files.path = $$OUT_PWD
COPIES += copy_files copy_dir

FORMS += \
    mainWindow.ui
