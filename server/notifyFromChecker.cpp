﻿#include "notifyFromChecker.h"
#include "notifyFromTcpServer.h"
#include "topConfig.h"
#include "notifyWidgetManager.h"
#include <QApplication>
#include <QDateTime>
#include <QString>
#include <QDebug>
#pragma execution_character_set("utf-8")
//#define CHECKER_TIMEOUT   (5*1000)
#define CHECKER_TIMEOUT   (1*1000)

void notifyFromChecker::initTimer(int period_seconds)
{
    timer1sForCheckingNotify = new QTimer(this);
    mPeriodSeconds = period_seconds;

    connect(timer1sForCheckingNotify, &QTimer::timeout, this, &notifyFromChecker::CheckForNoifty);

    //timer1sForCheckingNotify->start(mPeriodSeconds);
    //mIsEnable = true;
}

notifyFromChecker::notifyFromChecker()
{
    //QString CustNotifyConfigPath;
    QString application_path = QApplication::applicationDirPath();
    application_path = application_path.replace(QString('\\'), QString('/'));

    initTimer(CHECKER_TIMEOUT);
}

void notifyFromChecker::setEnable(bool logic)
{
    mIsEnable = logic;
    //qDebug() << "setEnable" << logic;
    if(mIsEnable == true)
    {
        //reload();
        timer1sForCheckingNotify->start(mPeriodSeconds);
        qDebug() << "start notifyFromChecker per " << mPeriodSeconds << "ms";
    }else
    {
        timer1sForCheckingNotify->stop();
        //reload();
        qDebug() << "stop notifyFromChecker";
    }
}

bool notifyFromChecker::getEnable(void)
{
    return mIsEnable;
}

void notifyFromChecker::CheckFromItems( struct ConfigItem * items, int itemCnt)
{
    int i;
    uint16_t flag;

    QDateTime curTime = QDateTime::currentDateTime();//获取系统现在的时间
    QLocale locale = QLocale::English;//指定英文显示

    //QString str = curTime.toString("MM-dd hh:mm:ss ddd"); //设置显示格式
    //QString str = locale.toString(curTime, "MM-dd hh:mm:ss ddd"); //设置显示格式
    QString curMon   = locale.toString(curTime, "MM");
    //QString curDay   = locale.toString(curTime, "dd"); （没必要）
    QString curHour  = locale.toString(curTime, "hh");
    QString curMin   = locale.toString(curTime, "mm");
    QString curSec   = locale.toString(curTime, "ss");
    QString curWek   = locale.toString(curTime, "ddd");

    //qDebug() <<  locale.toString(curTime, "MM");
    //qDebug() <<  curMon  ;
    //qDebug() <<  curDay  ;
    //qDebug() <<  curHour ;
    //qDebug() <<  curMin  ;
    //qDebug() <<  curSec  ;
    //qDebug() <<  curWek  ;

    //qDebug() <<str;
    for (i = 0; i < itemCnt; ++i)
    {
        flag = 0;

        if(QString::compare(tr("yes"), items[i].Enable, Qt::CaseInsensitive))
        {
            //qDebug() << "Skip : " << items[i].Description;
            continue;
        }
        foreach(QString mon, items[i].AlarmMonth)
        {
            //qDebug() << "mon.toInt" << mon.toInt();
            if(mon.toInt() == curMon.toInt())
            {
                //qDebug() << "ok: mon" << mon;
                flag |= MON_OK;
                //goto checkWek;
                break;
            }
        }
        //continue;

//checkWek:

        foreach(QString wek, items[i].AlarmWek)
        {
            //qDebug() << "wek.toLower" << wek.toLower();
            //if(wek.toLower() == curWek.toLower())
            if(!QString::compare(wek, curWek, Qt::CaseInsensitive))
            {
                //qDebug() << "ok:week" << wek;
                flag |= WEK_OK;
                break;
            }
        }

        foreach(QString hour, items[i].AlarmHour)
        {
            if(hour.toInt() == curHour.toInt())
            {
                //qDebug() << "ok: hour" << hour;
                flag |= HOUR_OK;
                break;
            }
        }

        foreach(QString mins, items[i].AlarmMin)
        {
            if(mins.toInt() == curMin.toInt())
            {
                //qDebug() << "ok:min" << mins;
                flag |= MIN_OK;
                break;
            }
        }

        foreach(QString secs, items[i].AlarmSec)
        {
            if(secs.toInt() == curSec.toInt())
            {
                //qDebug() << "ok:sec" << secs;
                flag |= SEC_OK;
                break;
            }
        }

        if(flag == (MON_OK | WEK_OK | HOUR_OK| MIN_OK | SEC_OK ))
        {
            //qDebug() << "mIsEnable" << mIsEnable << items[i].Type;
            if(mIsEnable == true)
            {
                NotifyManager::getInstance()->showSystemMessage(items[i].Tittle, items[i].Message,
                                                                items[i].URL,
                                                                items[i].During, items[i].Type);
                //qDebug() << "showNotifyWindows "<< "[" << items[i].Name << "] when " << str;
            }

            break;
        }
    }
}

void notifyFromChecker::CheckForNoifty(void)
{

#if 0
    QString time_format = "yyyy-MM-dd  HH:mm:ss";
    qDebug() << QDateTime::currentDateTime();
#endif
    if(mIsEnable != true)
    {
        return;
    }
    CheckFromItems(notifyFromCheckerPeriodicConfig::getInstance()->getNotifyItems(), notifyFromCheckerPeriodicConfig::getInstance()->getNotifyCnt());

#if 0
   if(timer1sForCheckingNotify->isActive())//如果定时器已经开启就关闭
   {
        timer1sForCheckingNotify->stop();//关闭定时器
   }
#endif

    //timer1sForCheckingNotify->start(CHECKER_TIMEOUT);
}
