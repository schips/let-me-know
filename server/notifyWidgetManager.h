#ifndef NOTIFYWIDGETMANAGER_H
#define NOTIFYWIDGETMANAGER_H

#include <QObject>
#include <QQueue>

#include "notifyWidget.h"

#include "topConfig.h"
class NotifyManager : public QObject
{
    Q_OBJECT
public:
    explicit NotifyManager( QObject *parent = 0);

    void setMaxCount(int count);
    void setDisplayTime(int ms);
    void showSystemMessage(QString & tittleArg, QString & bodyArg, QString & url, int durationSec, QString type);
    static NotifyManager *getInstance(void);
private:
    static void setInstance(NotifyManager * Manager);
    void notify(const QString &title, const QString &body, const QString &icon, const QString url, int displayTime);
    class NotifyData {
    public:
        NotifyData(const QString &icon, const QString &title, const QString &body, const QString url, int displayTime,QDateTime time):
          icon(icon),
          title(title),
          body(body),
          url(url),
          displayTime(displayTime),
          timeMark(time)
        {
        }

        QString icon;
        QString title;
        QString body;
        QString url;
        int displayTime;
        QDateTime timeMark;
    };

    void rearrange();
    void showNext();

    QQueue<NotifyData> dataQueue;
    QList<Notify*> notifyList;
    int maxCount;
    int displayTime;

    /* For display config*/
    topConfig* mtopConfig;
    int mWinHeight;
    int mWinWidth;
    int mWinGap;
};

#endif // NOTIFYWIDGETMANAGER_H
