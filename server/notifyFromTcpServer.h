﻿#if 1
#ifndef QTCP_H
#define QTCP_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

#include "topConfig.h"

class notifyFromTcpServer: public QObject
{
    Q_OBJECT
public:
    notifyFromTcpServer();
signals:
    //void messageIn(QString &meg);
    void messageIn(QString tittle, QString msg, QString & url, int  durationSec, QString type);
public slots:
    void acceptConnection();
    void requestIn(void);
    void disconnectHandle(void);
private:

    QTcpServer* m_tcpServer;
    QTcpSocket* m_tcpSocket;
    topConfig * mtopConfig;
    bool isEnableUrl;
    uint16_t bindPort;
    int mToken;
};

#endif // QTCP_H
#endif
